package com.restfulBooker.test.automation.request;

import io.restassured.RestAssured;

//Delete:it may have request body and response body
//200(OK),204(No content),202(Accepted)
public class DeleteBookingDemo 
{
	public static void main(String[] args) 
	{
		RestAssured
		.given()
		.log().all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking/6")
		.header("Content-Type", "application/json")
		.header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")

		.when()
		.delete()
		
		.then()
		.log().all()
		.statusCode(201);
	}

}
