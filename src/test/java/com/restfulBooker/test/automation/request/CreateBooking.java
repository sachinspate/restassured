package com.restfulBooker.test.automation.request;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class CreateBooking {

	public static void main(String[] args) {
		/*	1.Normal way	
		  // Build request 
		RequestSpecification rs = RestAssured.given();
		  rs.log().all(); rs.baseUri("https://restful-booker.herokuapp.com/");
		  rs.basePath("booking"); rs.body("{\r\n" + "    \"firstname\" : \"Jim\",\r\n"
		  + "    \"lastname\" : \"Brown\",\r\n" + "    \"totalprice\" : 111,\r\n" +
		  "    \"depositpaid\" : true,\r\n" + "    \"bookingdates\" : {\r\n" +
		  "        \"checkin\" : \"2018-01-01\",\r\n" +
		  "        \"checkout\" : \"2019-01-01\"\r\n" + "    },\r\n" +
		  "    \"additionalneeds\" : \"Breakfast\"\r\n" + "}");
		  
		  rs.contentType(ContentType.JSON);
		  
		  //Hit request and get the response 
		  Response res = rs.post();
		  
		  //Validate the response 
		  ValidatableResponse validatorResponse =
		  res.then().log().all(); validatorResponse.statusCode(200);
		 */
		
		/*
		//2.Method chaining
		RestAssured
		//Return type is same that is RequestSpecification
		.given()
		.log().all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
		.contentType(ContentType.JSON)
		.body("{\r\n" + "    \"firstname\" : \"Jim\",\r\n" + "    \"lastname\" : \"Brown\",\r\n"
						+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n"
						+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2018-01-01\",\r\n"
						+ "        \"checkout\" : \"2019-01-01\"\r\n" + "    },\r\n"
						+ "    \"additionalneeds\" : \"Breakfast\"\r\n" + "}")
				
		.post()
		.then()
		.log().all()
		.statusCode(200);*/
		
		
		//3.using BDD syntax		
		RestAssured
		.given()
		.log().all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
		.contentType(ContentType.JSON)
		.body("{\r\n" + "    \"firstname\" : \"Jim\",\r\n" + "    \"lastname\" : \"Brown\",\r\n"
						+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n"
						+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2018-01-01\",\r\n"
						+ "        \"checkout\" : \"2019-01-01\"\r\n" + "    },\r\n"
						+ "    \"additionalneeds\" : \"Breakfast\"\r\n" + "}")
		
		.when()
		.post()
		
		.then()
		.log().all()
		.statusCode(200);
	}

}
