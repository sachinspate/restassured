package com.restfulBooker.test.automation.request;

import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;

public class PathParameterDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Map<String,Object> pathParam=new HashMap<String, Object>();
		pathParam.put("basePath", "booking");
		pathParam.put("bookingid", 2);
		RestAssured
		.given()
		 .log().all()
		 .baseUri("https://restful-booker.herokuapp.com/")
		 .basePath("{basePath}/{bookingid}")
		 
		 //We can set the path parameter either in pathParam or we can give it in get method
				/*
				  .pathParam("basePath", "booking") 
				  .pathParam("bookingid", 2)
				 */
		 //.pathParam("basePath", "booking")
		 .pathParams(pathParam)
		 .when()
		// .get()
		// .get("https://restful-booker.herokuapp.com/{basePath}/{bookingid}","booking",2)
		 //.get("https://restful-booker.herokuapp.com/{basePath}/{bookingid}",2)
		 .get()
		 .then()
		 .log().all()
		 .statusCode(200);
	}

}
