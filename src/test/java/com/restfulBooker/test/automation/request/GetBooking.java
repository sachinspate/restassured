package com.restfulBooker.test.automation.request;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class GetBooking {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//https://restful-booker.herokuapp.com/booking/1
		RestAssured
		.given()
		.log().all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking/{id}")
		.pathParam("id", 1)
		.contentType(ContentType.JSON)
		
		.when()
		.get()
		
		.then()
		.log().all()
		.statusCode(200);
		
	}

}
