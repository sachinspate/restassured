package com.restfulBooker.test.automation.request;

import io.restassured.RestAssured;
//PUT-If resource found then it will replace the current representation of the resource with the new request payload
//if resource is not found on server,put is capable to create new resource 
//201,200 or 204 status code
public class UpdateBookingDemo {

	public static void main(String[] args) 
	{
		RestAssured
		.given()
		.log().all()
		.baseUri("https://restful-booker.herokuapp.com")
		.basePath("/booking/1")
		.header("Content-Type","application/json")
		.header("Authorization", "Basic YWRtaW46cGFzc3dvcmQxMjM=")
		.body("{\r\n" + 
				"    \"firstname\" : \"James\",\r\n" + 
				"    \"lastname\" : \"Brown\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2018-01-01\",\r\n" + 
				"        \"checkout\" : \"2019-01-01\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}")
		.when()
		.put()
		
		.then()
		.log().all()
		.statusCode(200);
	}

}
