package com.restfulBooker.test.automation.request;

import io.restassured.RestAssured;
//Patch:to update resource partially or completely
//It represents what parts of resource needs to be modified 
public class PatchBookingDemo 
{
	public static void main(String[] args) 
	{
	RestAssured
	.given()
	.log().all()
	.baseUri("https://restful-booker.herokuapp.com/")
	.basePath("booking/1")
	.header("Content-Type","application/json")
	.header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")
	.body("{\r\n" + 
			"    \"firstname\" : \"Sachin\",\r\n" + 
			"    \"lastname\" : \"Pate\"\r\n" + 
			"}")
    .when()
    .patch()
    
    .then()
    .log().all()
    .statusCode(200);
	}

}
